## Commerce Transaction Account

The Custom Payment Gateway module allows Drupal Commerce stores to integrate a custom payment method using a Commerce Transaction Account. This module provides a seamless payment experience for customers and offers flexibility by allowing customization of the payment gateway header, label, and description to align with your store's branding.

For a full description of the module visit:
https://www.drupal.org/project/transaction_payment_account


To submit bug reports and feature suggestions, or to track changes visit:
https://www.drupal.org/project/issues/transaction_payment_account

## Features:
- Integration of a Commerce Transaction Account as a payment method.
- Customizable header, label, and description for the payment gateway.
- Seamless configuration for site administrators.
- Saving of customer-provided information during checkout for order reference.
- Display of payment information on the order view page.
- Payment tab interface similar to Pay At Store with 'Receive' and 'Void' operations.
- Ability for stores to add Commerce Transaction Account payment to orders.
- Integration with the Manage Orders page, listing Commerce Transaction Account as a Payment Method in the filter.

## Installation:
- Download the Custom Payment Gateway module from Drupal.org.
- Place the module in the /modules directory of your Drupal site.
- Navigate to the Extend page (/admin/modules) in your Drupal site.
- Enable the Custom Payment Gateway module from the list of available modules.
- Once enabled, go to the Payment Methods configuration page (/admin/commerce/config/payment-gateways) to configure the Custom Payment Gateway.
- Configure the header, label, and description according to your preferences.
- Save the configuration settings.

## Configuration:
- Navigate to the Payment Methods configuration page (/admin/commerce/config/payment-gateways).
- Locate the Custom Payment Gateway option and click on the "Configure" link.
- Customize the header, label, and description fields to match your store's requirements.
- Save the configuration settings.

## Usage
- During checkout, customers will be presented with the option to pay using the Custom Payment Gateway.
- Upon selection, customers can proceed with providing necessary payment information.
- After completing the checkout process, the provided information will be saved with the order.
- Site administrators can view the payment information on the order view page.
- Store managers can manage orders and filter them by the Commerce Transaction Account payment method.

## MAINTAINERS
Pradeep Kumar (pkghang) - pkghang's Drupal profile

# Supporting organization

- Virasat Solutions - https://www.drupal.org/virasat-solutions

Expert in Drupal design, development, Theming, and migration, as well as UI/UX design which makes us
a one-stop Drupal development company.


