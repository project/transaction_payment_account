<?php

namespace Drupal\transaction_payment_account\PluginForm;

use Drupal\commerce\InlineFormManager;
use Drupal\commerce_payment\Entity\PaymentMethod;
use Drupal\commerce_payment\Exception\DeclineException;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\PluginForm\PaymentMethodFormBase;
use Drupal\commerce_store\CurrentStoreInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a class for transaction payment gateway plugin add form.
 */
class TransactionAccountMethodAddForm extends PaymentMethodFormBase {
  use StringTranslationTrait;
  /**
   * The route match service.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * Constructs a new TransactionAccountMethodAddForm object.
   *
   * @param \Drupal\commerce_store\CurrentStoreInterface $current_store
   *   The current store.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\commerce\InlineFormManager $inline_form_manager
   *   The inline form manager.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match service.
   */
  public function __construct(
    CurrentStoreInterface $current_store,
    EntityTypeManagerInterface $entity_type_manager,
    InlineFormManager $inline_form_manager,
    LoggerInterface $logger,
    RouteMatchInterface $route_match,
  ) {
    parent::__construct($current_store, $entity_type_manager, $inline_form_manager, $logger);
    $this->routeMatch = $route_match;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new self(
      $container->get('commerce_store.current_store'),
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.commerce_inline_form'),
      $container->get('logger.channel.commerce_payment'),
      $container->get('current_route_match')
    );
  }

  /**
   * This method is used to access the current route match.
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $current_route = $this->routeMatch->getRouteName();
    $form = parent::buildConfigurationForm($form, $form_state);
    /** @var \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method */
    $payment_method = $this->entity;
    if ($current_route === 'commerce_checkout.form') {

      $form['payment_details'] = [
        '#parents' => array_merge($form['#parents'], ['payment_details']),
        '#type' => 'container',
        '#payment_method_type' => $payment_method->bundle(),
      ];
      $form['payment_details'] = $this->buildTransactionAccountForm($form['payment_details'], $form_state);

      // Move the billing information below the payment details.
      if (isset($form['billing_information'])) {
        $form['billing_information']['#weight'] = 10;
      }
    }
    else {
      unset($form['billing_information']);
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $current_route = $this->routeMatch->getRouteName();

    try {
      if ($current_route === 'commerce_checkout.form') {
        parent::submitConfigurationForm($form, $form_state);

        $this->submitTransactionAccountForm($form['payment_details'], $form_state);
        $values = $form_state->getValue($form['#parents']);

        $payment_gateway_plugin = $this->plugin;

        $payment_gateway_plugin->createPaymentMethod($this->entity, $values['payment_details']);
      }
    }
    catch (DeclineException $e) {
      $this->logger->warning($e->getMessage());
      throw new DeclineException('We encountered an error processing your payment method. Please verify your details and try again.');
    }
    catch (PaymentGatewayException $e) {
      $this->logger->error($e->getMessage());
      throw new PaymentGatewayException('We encountered an unexpected error processing your payment method. Please try again later.');
    }
  }

  /**
   * Builds the transaction form.
   *
   * @param array $element
   *   The target element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the complete form.
   *
   * @return array
   *   The built transaction form.
   */
  protected function buildTransactionAccountForm(array $element, FormStateInterface $form_state) {
    $element['#attributes']['class'][] = 'transaction-form';
    $configuration = $this->plugin->getConfiguration();
    $display_label = $configuration['display_label'];
    $label = $configuration['transaction_account_number_label'];
    $desc = $configuration['transaction_account_number_description'];

    $element['transaction_account_title_and_description'] = [
      '#markup' => '<strong>' . $this->t($display_label) . '</strong><br />' . $this->t($desc),
    ];
    $transaction_account_number = '';
    $order = $this->routeMatch->getParameter('commerce_order');
    $paymentMethodId = $order->get('payment_method')->target_id;
    if ($paymentMethodId) {
      $paymentMethod = PaymentMethod::load($paymentMethodId);
      if ($paymentMethod) {
        $transaction_account_number = $paymentMethod->transaction_account_number->value;
      }
    }

    $element['transaction_account_number'] = [
      '#type' => 'textfield',
      '#title' => $this->t($label),
      '#default_value' => $transaction_account_number,
      '#attributes' => ['autocomplete' => 'off'],
      '#required' => TRUE,
      '#size' => 50,
    ];

    return $element;
  }

  /**
   * Handles the submission of the transaction form.
   *
   * @param array $element
   *   The transaction form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the complete form.
   */
  protected function submitTransactionAccountForm(array $element, FormStateInterface $form_state) {
    $values = $form_state->getValue($element['#parents']);

    $this->entity->transaction_account_number = $values['transaction_account_number'];
  }

}
