<?php

namespace Drupal\transaction_payment_account\PluginForm;

use Drupal\commerce_payment\PluginForm\OnsitePaymentAddForm;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a payment add form for Transaction Accounts.
 */
class TransactionAccountPaymentAddForm extends OnsitePaymentAddForm implements ContainerInjectionInterface {
  use StringTranslationTrait;
  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new TransactionAccountPaymentAddForm object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
  ) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new self(
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;
    $order = $payment->getOrder();
    if (!$order) {
      throw new \InvalidArgumentException('Payment entity with no order reference given to PaymentAddForm.');
    }
    // The payment amount should not exceed the remaining order balance.
    $balance = $order->getBalance();
    $amount = $balance->isPositive() ? $balance : $balance->multiply(0);
    $form['amount'] = [
      '#type' => 'commerce_price',
      '#title' => $this->t('Amount'),
      '#default_value' => $amount->toArray(),
      '#required' => TRUE,
    ];
    $configuration = $this->plugin->getConfiguration();
    $label = $configuration['transaction_account_number_label'];

    $form['transaction_account_number'] = [
      '#type' => 'textfield',
      '#title' => $this->t($label),
      '#attributes' => ['autocomplete' => 'off'],
      '#required' => TRUE,
      '#size' => 50,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {

    $values = $form_state->getValue('payment');
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;
    $payment->transaction_account_number = $values['transaction_account_number'];
    $payment->amount = $values['amount'];
    /** @var \Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OnsitePaymentGatewayInterface $payment_gateway_plugin */
    $payment_gateway_plugin = $this->plugin;
    $payment_method_storage = $this->entityTypeManager->getStorage('commerce_payment_method');
    $order = $payment->getOrder();

    $payment_method = $payment_method_storage->createForCustomer(
      'transaction_payment_account',
      $payment->getPaymentGateway()->id(),
      $order->getCustomer()->id()
    );

    $payment_method->transaction_account_number = $values['transaction_account_number'];
    $payment_method->setReusable(FALSE);
    $payment_method->save();
    $payment->set('payment_method', $payment_method);

    $payment_gateway_plugin->createPayment($payment, FALSE);
  }

}
