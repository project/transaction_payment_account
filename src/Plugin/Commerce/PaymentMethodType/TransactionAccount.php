<?php

namespace Drupal\transaction_payment_account\Plugin\Commerce\PaymentMethodType;

use Drupal\commerce_payment\Entity\PaymentGateway;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentMethodType\PaymentMethodTypeBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\entity\BundleFieldDefinition;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the Transaction Account payment method type.
 *
 * @CommercePaymentMethodType(
 *   id = "transaction_payment_account",
 *   label = @Translation("Transaction Account"),
 * )
 */
class TransactionAccount extends PaymentMethodTypeBase implements ContainerFactoryPluginInterface {

  /**
   * Constructs a new TransactionAccount object.
   *
   * @param array $configuration
   *   The plugin configuration, i.e. an array with configuration values keyed
   *   by configuration option name.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Routing\RouteMatchInterface $routeMatch
   *   The route match service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    protected RouteMatchInterface $routeMatch,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new self(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_route_match')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildLabel(PaymentMethodInterface $payment_method) {
    $payment_gateway = PaymentGateway::load('transaction_account');
    $display_label = $payment_gateway->getPlugin()->getDisplayLabel() ?? $payment_gateway->label();

    $args = [
      '@transaction_account_number' => $payment_method->transaction_account_number->value,
      '@display_label' => $display_label,
    ];
    if ($this->routeMatch->getRouteName() == 'entity.commerce_order.canonical') {
      return $this->t($display_label);
    }
    else {
      return $this->t('@display_label (@transaction_account_number)', $args);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildFieldDefinitions() {
    $fields = parent::buildFieldDefinitions();

    $fields['transaction_account_number'] = BundleFieldDefinition::create('string')
      ->setLabel($this->t('Transaction Account Number'))
      ->setDescription($this->t('If you have a store account with us, you may use this payment method.'))
      ->setRequired(TRUE);

    return $fields;
  }

}
