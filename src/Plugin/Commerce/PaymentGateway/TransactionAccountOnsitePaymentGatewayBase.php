<?php

namespace Drupal\transaction_payment_account\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OnsitePaymentGatewayBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides the base class for on-site payment gateways.
 */
abstract class TransactionAccountOnsitePaymentGatewayBase extends OnsitePaymentGatewayBase {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['display_label']['#access'] = TRUE;
    return $form;

  }

}
